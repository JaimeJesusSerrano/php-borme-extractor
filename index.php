<?php

$loader = require __DIR__ . '/vendor/autoload.php';
use BormeExtractor\entities\BormeProcessor;
use BormeExtractor\entities\BormeDownloader;

    $BORME = <<<EOD
            10334 - AYESA ENGINEERING SA.
            Nombramientos. Apoderado: DEL REAL URBANO GERMAN. Datos registrales. T 4665 , F 210,
            S 8, H SE 60039, I/A 38 (23.12.16).
            
            10338 - AVONER PS CASTUERA SOCIEDAD LIMITADA.
            Ceses/Dimisiones. Adm. Unico: ASOMADILLA SOLAR 2007 SOCIEDAD LIMITADA. Socio
            único: ASOMADILLA SOLAR 2007 SOCIEDAD LIMITADA. Disolución. Fusion. Extinción. Datos
            registrales. T 5233 , F 155, S 8, H SE 86171, I/A 4 (30.12.16).
            
            10339 - BLC PASOL DE ENERGIAS SOCIEDAD LIMITADA.
            Ceses/Dimisiones. Adm. Unico: ASOMADILLA SOLAR 2007 SOCIEDAD LIMITADA. Socio
            único: ASOMADILLA SOLAR 2007 SOCIEDAD LIMITADA. Disolución. Fusion. Extinción. Datos
            registrales. T 5228 , F 15, S 8, H SE 86035, I/A 4 (30.12.16).
            
            10340 - ENERGIAS PCRV DE LA SERENA SOCIEDAD LIMITADA.
            Ceses/Dimisiones. Adm. Unico: ASOMADILLA SOLAR 2007 SOCIEDAD LIMITADA. Socio
            único: ASOMADILLA SOLAR 2007 SOCIEDAD LIMITADA. Disolución. Fusion. Extinción. Datos
            registrales. T 5233 , F 99, S 8, H SE 86159, I/A 4 (30.12.16).
            
            10341 - EXPLOTACION SOLAR PLANTA ASOMADILLA SOCIEDAD LIMITADA.
            Ceses/Dimisiones. Adm. Unico: ASOMADILLA SOLAR 2007 SOCIEDAD LIMITADA. Socio
            único: ASOMADILLA SOLAR 2007 SOCIEDAD LIMITADA. Disolución. Fusion. Extinción. Datos
            registrales. T 5233 , F 165, S 8, H SE 86179, I/A 4 (30.12.16).
EOD;

    $bromeProcessor = new BormeProcessor();
    var_dump($bromeProcessor->getBormeInformation($BORME));

    $url = 'https://www.boe.es/borme/dias/2017/01/10/pdfs/BORME-A-2017-6-41.pdf';
    $bormeDownloader = new BormeDownloader();
    var_dump($bormeDownloader->downloadBorme($url));