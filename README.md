# php-borme-extractor
# ====================
A PHP project to operate with Borme (Boletín Oficial del Registro Mercantil)

## Pre configuration
### Ensure PHP 7.3 is installed, else install it
```Shell
$ php -v
```

### Install vendor dependencies
```Shell
$ cd rootProject/
$ php composer.phar install
```

## To execute the example:
```Shell
$ php -f index.php > out.txt
```

## To execute tests:
```Shell
$ ./vendor/bin/phpunit src/tests
```