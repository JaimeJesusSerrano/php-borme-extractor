<?php
namespace BormeExtractor\entities;

class BormeProcessor
{
    /**
     * Returns array of Society entities from the text of a Borme.
     *
     * @return array Society[]
     */
    public function getBormeInformation(string $rawBormeText)
    {
        $arrayBormeText = $this->getConversionRawToArray($rawBormeText);

        $societies = array();
        foreach ($arrayBormeText as $singleBormeText) {
            $singleBormeText = str_replace(PHP_EOL, "", $singleBormeText);
            $society = $this->getSocietyFromSingleBormeText($singleBormeText);

            if (!empty($society)) {
                $societies[] = $society;
            }
        }
        return $societies;
    }

    private function getConversionRawToArray(string $rawBormeText)
    {
        $arrayRawText = explode(PHP_EOL, $rawBormeText);
        $arrayBormeText = array();

        $currentElementString = '';
        foreach ($arrayRawText as $singleRawText) {
            if (trim($singleRawText) != '') {
                $currentElementString .= ' '.trim($singleRawText);
            } else {
                $arrayBormeText[] = $currentElementString;
                $currentElementString = '';
            }
        }

        return $arrayBormeText;
    }

    private function getSocietyFromSingleBormeText(string $singleBormeText)
    {
        $splitSingleBormeText = $this->getSplitSingleBormeText($singleBormeText);
        if (!empty($splitSingleBormeText)) {
            $society = new Society();
            $society->setName($splitSingleBormeText[1]);
            $society->setVolume($splitSingleBormeText[3]);
            $society->setFolio($splitSingleBormeText[4]);
            $society->setSection($splitSingleBormeText[5]);
            $society->setSheet($splitSingleBormeText[6]);

            return $society;
        } else {
            return null;
        }
    }

    /*
     * Use example:
     * $singleBormeText: 10334 - AYESA ENGINEERING SA. Nombramientos. Apoderado: DEL REAL URBANO GERMAN. Datos registrales.
     * $matches:
        Full match	'10334 - AYESA ENGINEERING SA. Nombramientos. Apoderado: DEL REAL URBANO GERMAN. Datos registrales. T 4665 , F 210, S 8, H SE 60039,'
        Group 1.	'10334 - AYESA ENGINEERING SA'
        Group 2.	' Nombramientos. Apoderado: DEL REAL URBANO GERMAN. Datos registrales.'
        Group 3.	'4665'
        Group 4.	'210'
        Group 5.	'8'
        Group 6.	'SE 60039'
     */
    private function getSplitSingleBormeText(String $singleBormeText)
    {
        $matches = array();

        // '10334 - AYESA ENGINEERING SA'
        $pattern = '/^(.*?)\.';

        // ' Nombramientos. Apoderado: DEL REAL URBANO GERMAN. Datos registrales.'
        $pattern .= '(.*)';

        // '4665'
        $pattern .= '\sT\s(.*?)\s\,';

        // '210'
        $pattern .= '\sF\s(.*?)\,';

        // '8'
        $pattern .= '\sS\s(.*?)\,';

        // 'SE 60039'
        $pattern .= '\s(.*?)\,/';
        preg_match($pattern, $singleBormeText, $matches);

        return $matches;
    }
}