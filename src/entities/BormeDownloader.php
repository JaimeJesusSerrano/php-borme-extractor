<?php
namespace BormeExtractor\entities;

use Smalot\PdfParser\Parser;

class BormeDownloader
{
    const NUMBER_OF_ATTEMPTS = 10;
    const SECONDS_TO_WAIT_BETWEEN_ATTEMPTS = 1;

    /**
     * Save a file with the content of a Borme pdf from the url that has been introduced.
     *
     * @return string PDF text
     */
    public function downloadBorme(string $url)
    {
        if (!$this->isBormeUrlCorrect($url)) {
            throw new Exception('The url entered is not correct.');
        }

        $pdf = $this->getContentBormeFromPDF($url);

        if (empty($pdf)) {
            throw new Exception('Download the PDF Borme failed.');
        }

        $name = $this->getNameFromURL($url);
        $localPdfURL = 'docs/in/'.$name.'.pdf';
        $localTxtURL = 'docs/in/'.$name.'.txt';
        file_put_contents($localPdfURL, $pdf);

        if (!file_exists($localPdfURL)) {
            throw new Exception('Could not save PDF Borme.');
        }

        $parser = new Parser();
        $pdfDownloaded = $parser->parseFile($localPdfURL);
        $text = $pdfDownloaded->getText();
        file_put_contents($localTxtURL, $text);

        if (file_exists($localTxtURL)) {
            unlink($localPdfURL);
            return $localTxtURL;
        } else {
            throw new Exception('Could not create .txt from Borme but .pdf has been saved.');
        }
    }

    private function getContentBormeFromPDF(string $url)
    {
        $attempts = 0;
        do {
            try
            {
                $pdf = @file_get_contents($url);
            } catch (Exception $e) {
                $attempts++;
                sleep(self::SECONDS_TO_WAIT_BETWEEN_ATTEMPTS);
                continue;
            }
            break;
        } while($attempts < self::NUMBER_OF_ATTEMPTS);

        return $pdf;
    }

    private function isBormeUrlCorrect(string $url)
    {
        $matches = array();
        preg_match('/(https:\/\/www\.boe\.es\/borme\/dias\/)\d{4}\/\d{2}\/\d{2}\/pdfs\/BORME-\D-\d{4}-\d{1,}-\d{1,}.pdf$/', $url, $matches);

        if (empty($matches[0])) {
            return false;
        } else {
            return true;
        }
    }

    private function getNameFromURL(string $url)
    {
        $matches = array();
        preg_match('/pdfs\/(.*).pdf$/', $url, $matches);

        if (empty($matches[1])) {
            return 'no-name';
        } else {
            return $matches[1];
        }
    }
}